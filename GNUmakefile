# apito-fastapi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-fastapi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-fastapi. If not, see <https://www.gnu.org/licenses/>.

# DEPENDENCIES: podman, gawk, fzf, guix, curl

# LOAD ENV FILES
-include envs/env.*

.DEFAULT_GOAL := test

NAME := apito-fastapi
VERSION := $(shell gawk '/version/ {version=substr($$3, 2,5); print version}' pyproject.toml)
RUNNER ?= podman
IMAGE_NAME=${GITLAB_REGISTRY}/${USER}/${NAME}:${VERSION}
BACKEND_DIR=/app

# ------------------------------------
up: image.initial image.database image.start #server

down:
	${RUNNER} pod rm --force --ignore ${POD_NAME}
	${RUNNER} container rm --force --ignore ${NAME}
	${RUNNER} container rm --force --ignore ${DATABASE_NAME}
	${RUNNER} volume rm --force ${DATABASE_DATA}

# --------------------------- IMAGES

.PHONY: image.initial
image.initial:
	${RUNNER} pod create \
		--publish ${FRONTEND_PORT}:${FRONTEND_INTERNAL_PORT} \
		--publish ${BACKEND_PORT}:${BACKEND_INTERNAL_PORT} \
		--name ${POD_NAME}

.PHONY: image.stats
image.stats:
	${RUNNER} pod stats ${POD_NAME}

.PHONY: image.database
image.database:
	${RUNNER} rm -f ${DATABASE_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--detach \
		--pod ${POD_NAME} \
		--name ${DATABASE_NAME} \
		--env POSTGRES_PASSWORD=${SQL_PASSWORD} \
		--env POSTGRES_USER=${SQL_USERNAME} \
		--env POSTGRES_DB=${SQL_DATABASE} \
		--volume ${DATABASE_DATA}:${SQL_DATA}:Z \
		${DATABASE_IMAGE}

.PHONY: image.server
image.server:
	${RUNNER} rm -f ${SERVER_NAME}
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--detach \
		--restart=unless-stopped \
		--name ${SERVER_NAME} \
		--volume ${PWD}:${BACKEND_DIR}:Z \
		--volume ./ops/nginx-default.conf:/etc/nginx/conf.d/default.conf:Z \
		${SERVER_IMAGE}

.PHONY: image.prod
image.prod:
	${RUNNER} rm -f ${NAME}-prod
	${RUNNER} run ${RUNNER_STATS} \
		--rm \
		--detach \
		--name ${NAME}-prod \
		${IMAGE_NAME}

.PHONY: image.start
image.start:
	${RUNNER} rm -f ${NAME}-start
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-start \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}:${BACKEND_DIR}:Z \
		--workdir ${BACKEND_DIR} \
		${IMAGE_NAME}

.PHONY: image.repl
image.repl:
	${RUNNER} rm -f ${NAME}-repl
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-repl \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}:${BACKEND_DIR}:Z \
		--workdir ${BACKEND_DIR} \
		${IMAGE_NAME} \
		bash

.PHONY: image.test.unit
image.test.unit:
	${RUNNER} rm -f ${NAME}-test-unit
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test-unit \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}:${BACKEND_DIR}:Z \
		--workdir ${BACKEND_DIR} \
		${IMAGE_NAME} \
		bash -c "composer run test:unit -n"

.PHONY: image.test.integration
image.test.integration:
	${RUNNER} rm -f ${NAME}-test-integration
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test-integration \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}:${BACKEND_DIR}:Z \
		--workdir ${BACKEND_DIR} \
		${IMAGE_NAME} \
		bash -c "./bin/test && composer run test:integration -n"

.PHONY: image.test.all
image.test.all:
	${RUNNER} rm -f ${NAME}-test-integration
	${RUNNER} run ${RUNNER_STATS} \
		--pod ${POD_NAME} \
		--rm --tty --interactive \
		--name ${NAME}-test-integration \
		--env DATABASE_URL="${SQL_ENGINE}://${SQL_USERNAME}:${SQL_PASSWORD}@${SQL_HOST}:${SQL_PORT}/${SQL_DATABASE}?serverVersion=${SQL_VERSION}&charset=utf8" \
		--volume ${PWD}:${BACKEND_DIR}:Z \
		--workdir ${BACKEND_DIR} \
		${IMAGE_NAME} \
		bash -c "composer run tests -n"

.PHONY: image.db.repl
image.db.repl:
	podman exec -it ${DATABASE_NAME} ${SQL_REPL_CMD}

.PHONY: image.commands
image.commands:
	${RUNNER} run \
		--rm --tty --interactive \
		--volume ${PWD}:${BACKEND_DIR}:Z \
		--workdir ${BACKEND_DIR} \
		${IMAGE_NAME} \
		bash -c '$(shell cat container-commands | fzf)'

.PHONY: image.build
image.build:
	${RUNNER} build \
		--file ./Dockerfile \
		--tag ${IMAGE_NAME}

.PHONY: image.publish
image.publish:
	${RUNNER} push ${IMAGE_NAME}

# ------------------------------ LOCAL

.phony: local.install
local.install:
	python3 -m pip install . --break-system-packages

.phony: local.uninstall
local.uninstall:
	python3 -m pip uninstall onur --break-system-packages

.PHONY: local.env
local.env:
	python -m venv venv
	bash ./venv/bin/activate
	./venv/bin/pip install --upgrade pip
	./venv/bin/pip install .

.PHONY: local.commands
local.commands:
	cat ./container-commands | fzf

.PHONY: local.clean
local.clean:
	rm -rf ./venv ./build ./dist ./.pytest_cache ./onur.egg-info
	find ./onur -type d -name '__pycache__' -exec rm -r {} +

.PHONY: local.openapi
local.openapi:
	curl localhost:5000/openapi.json > docs/openapi/${NAME}-openapi-${VERSION}.json

.PHONY: local.guix
local.guix:
	guix shell --pure --container
