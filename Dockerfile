# apito-fastapi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# apito-fastapi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with apito-fastapi. If not, see <https://www.gnu.org/licenses/>.

FROM python:3.13
MAINTAINER EAS Barbosa <easbarba@outlook.com>

# ENV NEWUSER easbarba
# ENV APP_HOME /home/$NEWUSER/app \
# ENV PATH /home/$NEWUSER/.local/bin:$PATH
# RUN adduser --system --group $NEWUSER # RUN useradd -ms /bin/bash $NEWUSER && chown -R $USER_NAME:$USER_NAME  /home/$NEWUSER
# USER $NEWUSER
# WORKDIR $APP_HOME

WORKDIR /app

ENV PYTHONUNBUFFERED=1 \
    PYTHONDONTWRITEBYTECODE=1

COPY ./pyproject.toml LICENSE README.md .
RUN python3 -m pip install --upgrade pip && \
    python3 -m pip install --no-cache-dir .

COPY . .

CMD [ "python3", "-m", "apito" ]
# CMD [ "uvicorn", "apito.app:app", "--host", "::", "--port", "8080", "--log-level", "debug", "--reload" ]
