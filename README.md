<!--
 apito-fastapi is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.

 apito-fastapi is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with apito-fastapi. If not, see <https://www.gnu.org/licenses/>.
-->

# Apito | FastAPI

Evaluate soccer referees' performance.

[JakartaEE](https://gitlab.com/easbarba/apito-jakartaee) | [Spring Boot](https://gitlab.com/easbarba/apito-spring) | [Quarkus](https://gitlab.com/easbarba/apito-quarkus) | [Symfony](https://gitlab.com/easbarba/apito-symfony) | [Vue.js](https://gitlab.com/easbarba/apito-vue) | [Main](https://gitlab.com/easbarba/apito) 

## Techs

- [FastAPI](https://jakarta.ee): Persistence, Validation, Rest.
- [EclipseLink](https://eclipse.dev/eclipselink)
- [Flyway](https://flywaydb.org/): Versioning database deployment 
- [Apache Maven](https://maven.apache.org/)
- [MapStruct](https://mapstruct.org/): DTO Bean mappings
- [PostgreSQL](https://www.postgresql.orgf/) 
- [Keycloak](https://www.keycloak.org) via container
- [TestContainers](https://testcontainers.com)
- [Koto](https://gitlab.com/easbarba/koto): in-house CLI tool that leverages UNIX tools to reproduce `insomnia` API testings features.
- [Makefile](Makefile) : speed up development some `make` rules are provided. 
- [GNU Guix](https://guix.gnu.org): install all system dependencies in easy undo isolated experience.
- [Podman](https://podman.io) [pods](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods#shortcut_to_create_pods) that offers a rootless k8s's pods like experience to local development!

| Endpoints            | Description                   |
|----------------------|-------------------------------|
| /api//VERSION        | Apito general information     |
| /api/VERSION/openapi | openAPI documentation as json |
| /api/VERSION/swagger | openAPI UI swagger            |


## Resources

| Endpoints             | Methods                               |
|-----------------------|---------------------------------------|
| /api/VERSION/referees | OPTION, GET, POST, PUT, PATCH, DELETE |
    
PS: Non-Safe methods require a bearer token. 

## [Documentation](docs)

Additional Information about API design, openAPI, and related can be found in the `docs` directory.

![podman pod](podman_pod.png)

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
<!--
 apito-jakartaee is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 apito-jakartaee is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with apito-jakartaee. If not, see <https://www.gnu.org/licenses/>.
-->

# Apito | Jakarta EE

Evaluate soccer referees' performance.

[Symfony](https://gitlab.com/easbarba/apito-symfony) [Express](https://gitlab.com/easbarba/apito-express) |  [Vue.js](https://gitlab.com/easbarba/apito-vue) | [Quarkus](https://gitlab.com/easbarba/apito-quarkus) |  [Main](https://gitlab.com/easbarba/apito)

## [Documentation](docs)

All information about API design, openAPI, and related documentation are found at the `docs`.

## [Makefile](Makefile)

The Makefile file provides all sort of handy tasks. It relies on the `.env.*`
files, so make sure to match due variables.

| targets                | description                                           |
|------------------------|-------------------------------------------------------|
| up                     | spin up containers to development (synced folders)    |
| down                   | shutdown spinned containers                           |
| image.exec             | run commands inside  development container            |
| image.build            | build development container image                     |
| image.publish          | push to registry current development  container image |
| image.test.integration | run integration tests                                 |
| image.test.unit        | run unit tests                                        |

## [Podman Pods](https://podman.io)

Podman's pod offers a rootless k8s's pods-like experience to local development, [check it out!](https://developers.redhat.com/blog/2019/01/15/podman-managing-containers-pods#shortcut_to_create_pods)

Some targets are using pods to boot up necessary containers, look at the `Makefile` for more infromation.

![podman pod](./docs/podman_pod.png)

For more information on development check out the `CONTRIBUTING.md` document.

## [apitest](apitest)

A custom script is provided, as alternative, to test all endpoints easily, it has a few dependencies listed in its top.

    ./koto
    ./koto --id 29fe4068-fdf8-4775-acc0-140c4d066612

## LICENSE

[GNU GENERAL PUBLIC LICENSE Version 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
